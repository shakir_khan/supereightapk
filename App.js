import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Alert,
  TouchableOpacity,
  WebView,
  Dimensions ,
  BackHandler,
  ActivityIndicator
} from "react-native";
import {AntDesign,Entypo} from '@expo/vector-icons';
import Spinner from 'react-native-loading-spinner-overlay';

const width = Dimensions.get('window').width
const height =Dimensions.get('window').height

export default class Maps extends Component {
  state = {
    WEBVIEW_REF: "weViewRed",
    loading: false,
    key:0,
    canGoBack: false,
    back:true
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    // Don't forget to remove the listeners!
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
  }
  onBackPress = () => {
    const {canGoBack}=this.state
    if (!canGoBack) {
      Alert.alert(
        'Exit App',
        'Exiting the application?', [{
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
        }, {
            text: 'OK',
            onPress: () => BackHandler.exitApp()
        }, ], {
            cancelable: false
        }
     )
    } else {
      // console.log('new')
      // Navigation.pop(this.props.currentScreen.id);
      this.goBack()
    }
  
    return true;
  }
  goBack = () => {
    this.refs[this.state.WEBVIEW_REF].goBack();
  };
  goForward = () => {
    this.refs[this.state.WEBVIEW_REF].goForward();
  };
  renderLoadingView() {
    return (
        <ActivityIndicator
           animating = {this.state.loading}
           color = '#bc2b78'
           size = "large"
           style = {styles.icon}
           hidesWhenStopped={true} 
        />
    );
  }
  onNavigationStateChange(navState) {
    this.setState({
    canGoBack: navState.canGoBack
 });
}
  render() {
    console.log(this.state.loading)
    return (
      <View style={{ flex: 1 ,position:'relative',marginTop: 30,}}>
          <Spinner
              visible={this.state.loading}
              textContent={'Loading...'}
              textStyle={{ color: '#FFF' }}
          />
     
        
        <WebView
          source={{ uri: "http://tic.supereight.com.np/index.php" }}
          ref={this.state.WEBVIEW_REF}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)} 
          // renderLoading={this.renderLoadingView.bind(this)} startInLoadingState={true}
          onLoadStart={() => this.setState({ loading: true })}
          onLoadEnd={() => this.setState({ loading: false })}
          // onLoad={() => this.setState({ loading: false })}
        />
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => this.goBack()}
            // disabled={this.state.canGoBack}
          >
            <AntDesign name="caretleft"
                            size={35}
                            color={"#328173"}
                            style={styles.icon}
                             />
          </TouchableOpacity>
          <TouchableOpacity  onPress={()=>{ this.refs[this.state.WEBVIEW_REF] && this.refs[this.state.WEBVIEW_REF].reload();}}>
          <AntDesign name="reload1"
                            size={35}
                            color={"#328173"}
                            style={styles.icon}
                             />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.goForward()}>
            <AntDesign name="caretright"
                            size={35}
                            color={"#328173"}
                            style={styles.icon}
                             />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const Header = ({ loading }) => (
  loading?
  <View style={styles.header}>
    {/* <Text style={styles.title}>Profile</Text> */}
    {loading?<ActivityIndicator color="blue" />:null}
  </View>:null
);

const Footer = context => (
  <View style={styles.footer}>
    <TouchableOpacity>
      <Text style={styles.icon}>⬅️</Text>
    </TouchableOpacity>
    <TouchableOpacity>
      <Text style={styles.icon}>⭐️</Text>
    </TouchableOpacity>
    <TouchableOpacity>
      <Text style={styles.icon}> ➡️</Text>
    </TouchableOpacity>
  </View>
);
const styles = StyleSheet.create({
  header: {
    // position:'absolute',
    // top:0,
    // left:0,
    width:width,
    height:60,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: "rgba(0,0,0,0.9)"
  },
  title: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  icon: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  footer: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "#444"
  }
});